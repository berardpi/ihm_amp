#!/bin/bash

rm -f target/*
cd src

for f in *.html
do
        cat ../_header.html > ../target/$f
        cat $f >> ../target/$f
        cat ../_footer.html >> ../target/$f
done

for f in *.css
do
		cp $f ../target/$f
done

for f in *.js
do
		cp $f ../target/$f
done

#index
cat ../_header_not_connected.html > ../target/index.html
cat index/index.html >> ../target/index.html
    cat ../_footer.html >> ../target/index.html

#mes sports
cat ../_header.html > ../target/mes_sports.html
cat mes_sports/_header_mes_sports.html >> ../target/mes_sports.html
cat mes_sports/_main_mes_sports.html >> ../target/mes_sports.html
cat ../_footer.html >> ../target/mes_sports.html

cat ../_header.html > ../target/mes_sports_connect.html
cat mes_sports/_header_mes_sports.html >> ../target/mes_sports_connect.html
cat mes_sports/_alert_mes_sports.html >> ../target/mes_sports_connect.html
cat mes_sports/_main_mes_sports.html >> ../target/mes_sports_connect.html
cat ../_footer.html >> ../target/mes_sports_connect.html


cat ../_header.html > ../target/event.html
cat mes_sports/_header_mes_sports.html >> ../target/event.html
cat mes_sports/event.html >> ../target/event.html
cat ../_footer.html >> ../target/event.html

cat ../_header.html > ../target/mes_sports_abs.html
cat mes_sports/_header_mes_sports.html >> ../target/mes_sports_abs.html
cat mes_sports/_alert_abs_mes_sports.html >> ../target/mes_sports_abs.html
cat mes_sports/_main_mes_sports.html >> ../target/mes_sports_abs.html
cat ../_footer.html >> ../target/mes_sports_abs.html

