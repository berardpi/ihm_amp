$('#dropdown-btn').click(function(){
    $('.criterias-menu').slideDown();
    $('#dropdown-btn').fadeOut();
});

$('#dropup-btn').click(function(){
    $('#dropdown-btn').fadeIn();
    $('.criterias-menu').slideUp();
});

$('.horaire').click(function(){
    // Update the informations about the schedule
    $('.nfo-empty').fadeOut(0);
    $('.nfo-horaire').fadeIn(400);
    // Change the case color
    $(this).css('background-color', "#42ACDD");
    // Create a subscription button
    $('.subscription-empty').fadeOut(0);
    $('.subscription-btn').fadeIn(400);
});

$('#individual-sub-btn').click(function(){
    $('#subscribe-title').fadeOut(100);
    $('.subscription-btn').fadeOut(100);
    $('#sub-finish').fadeIn(400);
    /*
    //$('.modal-title').fadeOut();
    $('.modal-body').fadeOut(400);
    $('.modal-body-finish').fadeIn(400);
    $('.modal-footer').children().fadeOut(400);
    $('.modal-footer-finish').fadeIn(400);
    */
});


$('#group-sub-btn').click(function(){
    //$('.modal-title').fadeOut();
    $('.modal-body').fadeOut(400);
    $('.modal-body-grp').fadeIn(400);
    $('.modal-footer').children().fadeOut(400);
    $('.modal-footer-grp').fadeIn(400);

});

$('#group-proposed-btn').click(function(){
    $('#subscribe-title').fadeOut(100);
    $('.subscription-btn').fadeOut(100);
    $('#sub-group-added').fadeIn(400);
});

$('#add-mbr-btn').click(function(){
    $('#grp').append('<li>'+
		     $('#member').val()
		     +'<button type="button" class="close del-member-btn" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>');
});

$(document).on('click', '.del-member-btn', function() {
    $(this).parent().remove();
});

$(document).on('click', '#subscribe-compet', function() {
    $('#subscribe-compet').fadeOut(100);
    $('#sub-compet-finish').fadeIn(400);
    $('#unsubscribe-compet').fadeIn(100);
});

$(document).on('click', '#unsubscribe-compet', function() {
    $('#sub-compet-finish').fadeOut(400);
    $('#unsubscribe-compet').fadeOut(100);
    $('#subscribe-compet').fadeIn(100);
});
